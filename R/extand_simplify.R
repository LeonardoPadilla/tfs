extand_simplify = function(df, variable = as.character(), unique = T, sort = T) {
  if(length(variable) > 1) stop('only one variable is allowed')
  if(any(class(df) %in% 'sf')) {
    df = sf::st_drop_geometry(df)
  }
  df = df[, variable]
  out = df |>
    as.matrix() |>
    as.vector()
  if(unique) out = unique(out)
  if(sort) out = sort(out)
  return(out)
}
# as_vector = function(df, variable = as.character()) {
#   if(length(variable) > 1) stop('only one variable is allowed')
#   df = df[, variable]
#   out = df |>
#     as.matrix() |>
#     as.vector()
#   return(out)
# }


