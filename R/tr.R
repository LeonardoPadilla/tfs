tr = function(matrix) {
  Out = as.matrix(matrix)
  if(nrow(Out) != ncol(Out)) {stop("ops ... non-square matrix")}
  return(sum(diag(Out)))
}
