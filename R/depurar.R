depurar = function(df, key_name, time, variable_name, replace = NA) {
  key_name_ = df[, 'key_name', drop = T]
  time_ = df[, 'time', drop = T]
  filtro = (key_name_ == key_name) & (time_ == time)
  df[filtro, variable_name] = replace
  return(df)
}