spatial_acf = function(h, reach, type = "exponential", derivative = 0L) {
  if(type == "exponential") {
    if(derivative == 0L) {return(exp(-h/reach))}
    if(derivative == 1L) {return(exp(-h/reach)*h/reach**2)}
    if(derivative == 2L) {return(exp(-h/reach)*(h**2/reach**4 - 2*h/reach**3))}
    else                 {"derivative must be 0, 1 or 2"}
  }
}
