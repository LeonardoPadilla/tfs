st_grf = function(formula, coords, time, beta, phi, reach, sigma2, nugget, type_spatial = 'exponential', type_temporal = 'ar') {
  options(na.action = "na.pass")
  S = st_cov(coords, time, phi, reach, sigma2, nugget, type_spatial, type_temporal)
  L = chol(S)
  e1 = t(L) %*% matrix(rnorm(nrow(S)))
  e2 = matrix(e1, ncol = time)
  
  data = as_stdata(coords = coords, data = e2)
  X = model.matrix(object = formula, data = data$data)
  
  Z = X %*% matrix(beta) + e1
  Z = matrix(Z, ncol = time)
  list("coords" = coords, "data" = Z)
}