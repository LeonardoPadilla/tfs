test_andersondarling = function(x) {
  out = test_normality(method = 'Anderson-Darling normality test')
  x = sort(x[complete.cases(x)])
  n = length(x)
  if (n < 8L)
    stop("sample size must be greater than 7")
  logp1 = pnorm((x - mean(x))/sd(x), log.p = TRUE)
  logp2 = pnorm(-(x - mean(x))/sd(x), log.p = TRUE)
  h = (2 * seq(1:n) - 1) * (logp1 + rev(logp2))
  A = -n - mean(h)
  AA = (1 + 0.75/n + 2.25/n^2) * A
  if (AA < 0.2) {
    p_value = 1 - exp(-13.436 + 101.14 * AA - 223.73 * AA^2)
  }
  else if (AA < 0.34) {
    p_value = 1 - exp(-8.318 + 42.796 * AA - 59.938 * AA^2)
  }
  else if (AA < 0.6) {
    p_value = exp(0.9177 - 4.279 * AA - 1.38 * AA^2)
  }
  else if (AA < 10) {
    p_value = exp(1.2937 - 5.709 * AA + 0.0186 * AA^2)
  }
  else p_value = 3.7e-24
  out@statistic = A
  out@p_value = p_value
  return(out)
}


