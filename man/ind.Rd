\name{ind}
\alias{ind}
\title{Extraction of the diagonal of a matrix}
\usage{
ind()
}
\description{
Devuelve una matriz diagonal de la diagonal de una matriz
}
\examples{
library(tfs)

A = matrix(data = c(1, 2, 3, 4), nrow = 2, ncol = 2)
in_diag(A)
}
