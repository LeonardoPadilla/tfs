\name{tr}
\alias{tr}
\title{Trace of a matrix}
\usage{
tr()
}
\description{
Calcula la traza de una matriz
}
\examples{
library(tfs)

A = matrix(data = c(1, 2, 3, 4), nrow = 2, ncol = 2)
tr(A)
}
